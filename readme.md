# Afrigold 

This software is a Website Application for an investment Company

## Software Requirements

This Software is built on Laravel framework has a few system requirements.
 Of course, all of these requirements are satisfied by the Laravel 
 Homestead virtual machine, so it's highly recommended that you use 
 Homestead as your local Laravel development environment.
 
 However, if you are not using Homestead, you will need to make sure your server meets the following requirements:
 
    PHP >= 7.1.3
    OpenSSL PHP Extension
    PDO PHP Extension
    Mbstring PHP Extension
    Tokenizer PHP Extension
    XML PHP Extension
    Ctype PHP Extension
    JSON PHP Extension

## Installation 

 Install the dependencies using the following command.
 
 ```
 composer install
 ```
 
 ```
 cp .evn.example .evn
 ```
  
```
 php artisan key:generate
``` 

```
 php artisan serve
```