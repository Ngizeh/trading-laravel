@extends('master')

@section('content')
<section class="container">
    <div class="row">
        <div class="col-md-5">
            <h4 class="navbar-font">Informed Choices, Timeless Advantage</h4>
            <p class="bold-font">Afrogold Investment Company is duly accredited intermediary of SBG securities,
                Leading stock brokerage firm in East Africa</p>
            <p class="bold-font">We were the first to give Main Street the chance to trade like Wall Street—and we're still delivering
                solutions today.
                Open a new brokerage or retirement account today.</p>

            <div>
                <h4 class="navbar-font">Quick links</h4>
                <ul>
                    <li class="no-list-style">
                        <a href="https://www.nse.co.ke/" target="_blank">
                            <span class="navbar-font bold-font">
                                Nairobi Securities Exchange
                            </span>
                        </a>
                    </li>
                    <li class="no-list-style">
                        <a href="http://www.sbgsecurities.co.ke/" target="_blank">
                            <span class="navbar-font bold-font">
                                Sbg Securites
                            </span>
                        </a>
                    </li>
                    <li class="no-list-style">
                        <a href="http://www.stanbicbank.co.ke/" target="_blank">
                            <span class="navbar-font bold-font">
                                Stanbic bank
                            </span>
                        </a>
                    </li>
                    <li class="no-list-style">
                        <a href="https://www.cma.or.ke/" target="_blank">
                            <span class="navbar-font bold-font">
                                Capital Markets Authority
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <img src="{{asset('images/trader-laptop.png')}}" alt="">
        </div>
    </div>
    {{--<h2 class="lead">Open a CDS account and Transfer to Us</h2>--}}
</section>
{{--<div class="container-fluid">--}}
{{--<section class="container second">--}}
{{--<div class="col-md-6 first">--}}
{{--<h3>Knowledgeable support when you need it</h3>--}}
{{--<p>When money’s at stake, you need answers fast. In addition to 24/7 Customer Service,--}}
{{--our Financial Consultants are on call—talk to one today.</p>--}}
{{--<a href="#" class="btn btn-default">Financial Consultant </a>--}}
{{--</div>--}}
{{--<div class="col-md-6 two">--}}
{{--<h3>Advanced security that never clocks out</h3>--}}
{{--<p>Your account security is paramount. Along with strong network defenses and encryption, we protect your--}}
{{--privacy, assets,--}}
{{--and every transaction you make with the Afrigold Complete Protection Guarantee.</p>--}}
{{--<a href="#" class="btn btn-default">Security Center </a>--}}
{{--</div>--}}
{{--</section>--}}
{{--</div>--}}
@stop
