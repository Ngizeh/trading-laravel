@extends('master')

@section('content')
<div class="container-fluid contact">
    <div class="container contacts">
    <p class="lead"> For more information </p>
    <div class="row">
        <div class="col-md-6 get_in">
            <h3><u>Get in touch </u></h3>
            <p> Aviators Business Park, Nyerere Road.</p>
            <p>Luther Plaza, 1st Floor, Left wing.</p>
            <p>P.O.Box 58339 - 00200</p>
            <p>Nairobi, Kenya</p>
            <p>Tel: +254 795 600090</p>
            <p> +254 795 600 060</p>
            <p>Email: geoffrey.ochieng@afrigold.co.ke</p>
        </div>
        <div class="col-md-6 border">
            <div class="form-group">
                <label class="control-label" for="inputDefault">First Name</label>
                <input class="form-control" id="inputDefault" placeholder="First Name" type="text">
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDefault">Second Name</label>
                <input class="form-control" id="inputDefault" placeholder="Second Name" type="text">
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDefault">Phone Number</label>
                <input class="form-control" id="inputDefault" placeholder="Phone Number" type="phone_number">
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDefault">Email</label>
                <input class="form-control" id="inputDefault" placeholder="Email" type="email">
            </div>
            <div class="form-group">
                <label class="control-label" for="inputDefault">Message/Enquiry/Thoughts</label>
                <textarea class="form-control" rows="3" id="textArea"></textarea>
            </div>
            <a href="#" class="btn btn-primary ">Send</a>
        </div>
    </div>
</div>
</div>
@stop