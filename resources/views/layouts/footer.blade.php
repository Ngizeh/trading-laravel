<div class="container-fluid bottom-foot">
    <div class=" container foot">
        <div class="col-md-4">
            <p class="navbar-font footer-title">About Us</p>
            <ul class="footer">
                <li class="footer-item">
                    <a href="#">
                        <span class="navbar-font">
                            Company Overview
                        </span>
                    </a>
                </li>
                <li class="footer-item">
                    <a href="#">
                        <span class="navbar-font">
                            Investor Relations
                        </span>
                    </a>
                </li>
                {{--<li class="footer-item"><a href="#">CareersPrivacy Statement </a></li>--}}
                <li class="footer-item">
                    <a href="#">
                        <span class="navbar-font">
                            About Us
                        </span>
                    </a>
                </li>
                {{--<li class="footer-item"><a href="#">Accessibility Afrigold</a></li>--}}
            </ul>
        </div>
        <div class="col-md-4 navbar-font">
            <p class="navbar-font footer-title">Services</p>
            <ul class="footer">
                <li class="footer-item"><a href="#">
                        <span class="navbar-font">
                          CDS A/C Opening
                        </span>
                        </a>
                </li>
                <li class="footer-item">
                    <a href="#">
                        <span class="navbar-font">
                            Shares - Buy & Sell
                        </span>
                    </a>
                </li>
                <li class="footer-item">
                    <a href="#">
                        <span class="navbar-font">
                            Immobiliztion
                        </span>
                    </a>
                </li>
                <li class="footer-item" >
                    <a href="#">
                        <span class="navbar-font">
                            Broker Transfer
                        </span>
                    </a>
                </li>
                <li class="footer-item" >
                    <a href="#">
                        <span class="navbar-font">
                            After Services
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <p class="navbar-font footer-title">Quick Links</p>
            <ul class="footer">
                <li class="no-list-style">
                    <a href="https://www.nse.co.ke/" target="_blank">
                        <span class="navbar-font">
                            Nairobi Securites Exchange
                        </span>
                    </a>

                </li>
                <li class="no-list-style">
                    <a href="http://www.sbgsecurities.co.ke/" target="_blank">
                        <span class="navbar-font">
                            Sbg Securites
                        </span>
                    </a>
                </li>
                <li class="no-list-style">
                    <a href="http://www.stanbicbank.co.ke/" target="_blank">
                        <span class="navbar-font">
                            Stanbic bank
                        </span>
                    </a>
                </li>
                <li class="no-list-style">
                    <a href="https://www.cma.or.ke/" target="_blank">
                        <span class="navbar-font">
                            Capital Markets Authority
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="footer-note" style="text-align: center">
    <div class="container-fluid down" style="margin-top: -1%;">
        {{--<img class="img-responsive"  width="100%" height="20px" src="{{asset('images/Footer1.svg')}}" alt="footer">--}}
        {{--<div class="container font">--}}
            {{--<i class="fa fa-home fa-3x" aria-hidden="true"></i>--}}
            {{--<i class="fa fa-facebook fa-3x" aria-hidden="true"></i>--}}
            {{--<i class="fa fa-twitter fa-3x" aria-hidden="true"></i>--}}
            {{--<i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i>--}}
        {{--</div>--}}
        <p class="bold-font black-font" style="padding-top: 1%;"> @Copyright Afrigold Investment all right Reserved 2017 </p>
    </div>
</div>

<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>