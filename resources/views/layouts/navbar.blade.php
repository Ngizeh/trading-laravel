<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand">
            <span class="navbar-font">
                Afrigold
            </span>
        </a>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li>
                <a href="{{url('/')}}">
                    <span  class="navbar-font">
                       Home
                    </span>
                </a>
            </li>
            <li><a href="{{url('our-services')}}">
                    <span class="navbar-font">
                        Our Services
                    </span>
                </a>
            </li>
            <li>
                <a href="{{url('/ost')}}">
                    <span class="navbar-font">
                        OST
                    </span>
                </a>
            </li>
            <li>
                <a href="{{url('/about-us')}}">
                    <span class="navbar-font">
                        About us
                    </span>
                </a>
            </li>
            <li>
                <a href="{{url('/community')}}">
                    <span class="navbar-font">
                        Community (CSG)
                    </span>
                </a>
            </li>
            <li>
                <a href="{{url('/contacts')}}">
                    <span class="navbar-font">
                        Contact us
                    </span>
                </a>
            </li>
        </ul>
        {{--<div class="nav navbar-nav side">--}}
            {{--<div class="btn-group ">--}}
                {{--<a type="button" class="btn btn-primary" >Open a CDS account</a>--}}
            {{--</div>--}}
            {{--&nbsp; &nbsp;--}}
            {{--<div class="btn-group">--}}
                {{--<a href="{{url('/login')}}" type="button" class="btn btn-default"> Sign in</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>