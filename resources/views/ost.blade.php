@extends('master')

@section('content')
<div class="container ost">
 <h2 class="navbar-font">Afrigold collaborates with ITrader to serve you</h2>
<p class="bold-font">The iTrader platform is built to increase accessibility to stock brokerage services. </p>
<p class="navbar-font">iTrader provides : </p>
    <ul>
        <li class="square-bullet ">
            <span class="bold-font">
                A HTTPS platform allowing for secure e-transactions
            </span>
        </li>
        <li class="square-bullet ">
            <span class="bold-font">
                24 hour access to share trading account
            </span>
        </li>
        <li class="square-bullet">
            <span class="bold-font">
                Real time market prices and order placement
            </span>
        </li>
        <li class="square-bullet">
            <span class="bold-font">
                Real time access to various reports such as portfolio valuations, transaction statements
            and stock holding reports
            </span>
        </li>
    </ul>
    <a href="http://www.csfs.co.ke/activelite/" class="btn btn-primary" target="_blank">Log in with ITrader</a>

</div>
    @stop