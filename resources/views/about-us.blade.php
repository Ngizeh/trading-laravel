@extends('master')
@section('content')
<div class="container-fluid color">
<div class="container about">
    <div class="row ">
        <div class="col-md-6">
            <h3><u>Who we are</u></h3>
            <p>Afrigold Investment Company is duly accredited intermediary of SBG Securities the leading
                stock brokerage in East Africa
            </p>
            <h3><u>Our Vision </u></h3>
            <p>We are here for you as investment companion</p>
            <h3><u>Our Mission</u></h3>
            <p>To help you invest any day any time</p>
            <h3><u>Our Values</u></h3>
            <p>Integrity, professionalism, Innovation, Quality service, Staff Empowerment and Teamwork</p>
        </div>
    </div>
</div>
</div>
@stop