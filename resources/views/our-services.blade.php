@extends('master')

@section('content')
<div class="container">
    <h4 class="navbar-font">CDS account opening</h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default" style="border: 10px solid whitesmoke;">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <span class="navbar-font">
                            Individuals account
                        </span>
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <ul>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            CDS and Opening Account (provided)
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            ID/Passport Copy
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            2 similar colour passport size photos
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Evidence of physical residence (utility bill not more than 3 months old )
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Evidence of Income (bank statement in your name or pay slip not more than 6 months)
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default" style="border: 10px solid whitesmoke;">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <span class="navbar-font">
                            Partnership
                        </span>
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <ul>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            CDS and Opening Account (provided)
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Certificate of Registration
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Partnership Agreement/Partnership Deed
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Written approval for account opening by partners
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            2 similar colour passport size photos, ID copy and personal Utility Bill
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Evidence of physical residence (utility bill not more than 3 months old )
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            PIN Certificate of Sole Proprietor (certified by any advocate)
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Evidence of Income (bank statement in your name or pay slip not more than 6 months)
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default" style="border: 10px solid whitesmoke;">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <span class="navbar-font">
                            Foreign Individuals/Diaspora
                        </span>
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <ul>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            CDS and Opening Account Form (provided)
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Certified passport/government issued ID
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            2 similar colour passport size photos
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Evidence of physical residence (utility bill not more than 3 months old )
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Evidence of Income (bank statement in your name or pay slip not more than 6 months)
                        </li>
                    </ul>
                    <p><strong>All document to be certified by notary public</strong></p>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default" style="border: 10px solid whitesmoke;">
        <div class="panel-heading" role="tab" id="headingFive">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <span class="navbar-font">
                            Sole Proprietorship
                        </span>
                </a>
            </h4>
        </div>
        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
            <div class="panel-body">
                <ul>
                    <li class="square-bullet" style="color: black; font-weight: bold;">
                        CDS 1 and account opening form (provided)
                    </li>
                    <li class="square-bullet" style="color: black; font-weight: bold;">
                        2 similar colour passport size photos, ID Copy and personal utility bill
                    </li>
                    <li class="square-bullet" style="color: black; font-weight: bold;">
                        Certificate of Registration
                    </li>
                    <li class="square-bullet" style="color: black; font-weight: bold;">
                        Evidence of physical address (utility bill not more than 3 months old)
                    </li>
                    <li class="square-bullet" style="color: black; font-weight: bold;">
                        PIN certificate of the sole proprietor (copy certified by any of our advocates )
                    </li>
                    <li class="square-bullet" style="color: black; font-weight: bold;">
                        Evidence of income of the business (bank statement in your name or pay slip not more than 6 months)
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <h4 class="navbar-font">Buying and Selling Shares</h4>
    <div class="panel panel-default" style="border: 10px solid whitesmoke;">
        <div class="panel-body" style="color: black; font-weight: bold;">
            To buy and sell share you need to have a CDS account with SBG securities.Click <a href="http://www.csfs.co.ke/activelite/">here</a> to
            register
            with ITrader (You must have an existing account with SBG and KYC compliant)
        </div>
    </div>
    <h4 class="navbar-font">Immobilization</h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default" style="border: 10px solid whitesmoke;">
            <div class="panel-heading" role="tab" id="headingSix">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                        <span class="navbar-font">
                            Requirements
                        </span>
                    </a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSIx">
                <div class="panel-body">
                    <ul>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Dully Filled CDS form signed by Shareholder
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Original share certificate to be immobilized - all certificate must be attached.Indemnity for
                            lost certificates to be attached where certificates are missing.
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Copy of ID/passport used to open to open an account/used to purchase the shares.Attach both if
                            different
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            CV statement and Oracle print shot
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <h4 class="navbar-font">Broker Transfer </h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default" style="border: 10px solid whitesmoke;">
            <div class="panel-heading" role="tab" id="headingSeven">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                        <span class="navbar-font">
                            Incoming transfers
                        </span>
                    </a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                <div class="panel-body">
                    <ul>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Dully Filled and signed transfer form from the current CDA
                            (incoming) by the client and the broker
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            CBGS internal transfer form signed by the client
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Copy of ID/PP used to open the account certified by the current broker and SBGS
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Certificate of incorporation (for companies) & ID/PP copies
                            of the signatories certified by the current broker and SBGS.
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Ensure that the account is opened/existing in Clear Vision.
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Clients must be KYC compliant.
                        </li>
                        <li class="square-bullet" style="color: black; font-weight: bold;">
                            Chargers: Ksh. 200 payable to CDSC
                        </li>
                    </ul>
                    <p>
                        <strong>
                            All attachments MUST be certified by SBGS staff.The originals need to
                            have been signed.
                        </strong>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>
    @stop